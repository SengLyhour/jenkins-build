#!/bin/sh

docker kill jenkins_ssl
docker rm jenkins_ssl
docker rmi jenkins_ssl
docker build -t jenkins_ssl . && \
docker run --name jenkins_ssl -d -p 9998:9998 --restart=always jenkins_ssl && \
docker logs -f jenkins_ssl